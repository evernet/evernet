import time
from datetime import datetime

import bcrypt
import jwt
from pymongo.collection import Collection

from node.service import NodeService
from lib.address import *


class ActorService:
    def __init__(self, mongo: Collection, node_service: NodeService, vertex: str):
        self.mongo = mongo
        self.node_service = node_service
        self.vertex = vertex

    def sign_up(self, node_identifier: str, identifier: str, password: str, display_name: str, actor_type: str,
                description: str) -> dict:
        if not identifier.isalnum():
            raise Exception("Identifier must be alphanumeric")
        if len(identifier) < 3 or len(identifier) > 64:
            raise Exception("Identifier must be between 3 and 64 characters")
        if len(password) < 8:
            raise Exception("Password must be at least 8 characters long")

        node = self.node_service.get(node_identifier)

        if self.identifier_exists(node["identifier"], identifier):
            raise Exception(f"Actor {identifier} already exists on node {node_identifier}")

        actor_id = self.mongo.insert_one({
            "node_identifier": node["identifier"],
            "identifier": identifier,
            "type": actor_type,
            "description": description,
            "password": bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode(),
            "display_name": display_name,
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
        }).inserted_id

        return {
            "id": str(actor_id),
            "identifier": identifier,
            "node_identifier": node_identifier,
        }

    def get_token(self, node_identifier: str, identifier: str, password: str, target_node_address: str | None) -> dict:
        private_key = self.node_service.get_signing_private_key(node_identifier)

        actor = self.mongo.find_one({
            "node_identifier": node_identifier,
            "identifier": identifier,
        })

        if not actor:
            raise Exception("Invalid login credentials")

        if not bcrypt.checkpw(password.encode(), actor["password"].encode()):
            raise Exception("Invalid login credentials")

        if not target_node_address:
            target_node_address = encode_node_address(node_identifier, self.vertex)

        token = jwt.encode({
            "sub": actor["identifier"],
            "type": "actor",
            "iss": encode_node_address(node_identifier, self.vertex),
            "aud": target_node_address,
            "iat": int(time.time()),
        }, headers={
            "kid": encode_node_address(node_identifier, self.vertex),
        }, algorithm="EdDSA", key=private_key)

        return {
            "token": token
        }

    def get(self, identifier: str, node_identifier: str) -> dict:
        actor = self.mongo.find_one({
            "node_identifier": node_identifier,
            "identifier": identifier,
        })

        if not actor:
            raise Exception(f"Actor {identifier} not found")

        return self.to_dict(actor)

    def update(self, identifier: str, actor_type: str, description: str, display_name: str, node_identifier: str) -> dict:
        fields = {
            "updated_at": datetime.now(),
        }

        if actor_type:
            fields["type"] = actor_type

        fields["description"] = description

        if display_name:
            fields["display_name"] = display_name

        result = self.mongo.update_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Actor {identifier} not found on node {node_identifier}")

        return {
            "identifier": identifier
        }

    def change_password(self, identifier: str, password: str, node_identifier: str) -> dict:
        if len(password) < 8:
            raise Exception("Password must be at least 8 characters long")

        fields = {
            "updated_at": datetime.now(),
        }

        if password:
            fields["password"] = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()

        result = self.mongo.update_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Actor {identifier} not found on node {node_identifier}")

        return {
            "identifier": identifier
        }

    def delete(self, identifier: str, node_identifier: str) -> dict:
        result = self.mongo.delete_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
        })

        if result.deleted_count == 0:
            raise Exception(f"Actor {identifier} not found on node {node_identifier}")

        return {
            "identifier": identifier,
            "node_identifier": node_identifier,
        }

    def identifier_exists(self, node_identifier: str, identifier: str) -> bool:
        return self.mongo.count_documents({
            "node_identifier": node_identifier,
            "identifier": identifier
        }) != 0

    @staticmethod
    def to_dict(self):
        return {
            "identifier": self["identifier"],
            "node_identifier": self["node_identifier"],
            "type": self["type"],
            "description": self["description"],
            "display_name": self["display_name"],
            "created_at": self["created_at"],
            "updated_at": self["updated_at"],
        }