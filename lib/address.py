def decode_node_address(node_address: str) -> (str, str):
    components = node_address.split('/')

    if len(components) != 2:
        raise ValueError(f'Invalid node address {node_address}')

    return components[1], components[0]


def encode_node_address(node_identifier: str, vertex: str) -> str:
    return f"{vertex}/{node_identifier}"


def decode_actor_address(actor_address: str) -> (str, str, str):
    components = actor_address.split("/")

    if len(components) != 3:
        raise Exception(f"Invalid actor address {actor_address}")

    return components[2], components[1], components[0]


def encode_actor_address(actor_identifier: str, node_identifier: str, vertex: str) -> str:
    return f"{vertex}/{node_identifier}/{actor_identifier}"


def encode_topic_address(topic_identifier: str, node_identifier: str, vertex: str) -> str:
    return f"{vertex}/{node_identifier}/{topic_identifier}"


def decode_topic_address(topic_address: str) -> (str, str, str):
    components = topic_address.split("/")

    if len(components) != 3:
        raise Exception(f"Invalid topic address {topic_address}")

    return components[2], components[1], components[0]

def decode_schema_address(schema_address: str) -> (str, str, str):
    components = schema_address.split("/")

    if len(components) != 3:
        raise Exception(f"Invalid schema address {schema_address}")

    return components[2], components[1], components[0]
