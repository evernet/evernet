from flask import Flask, render_template


class WebRoutes:
    def __init__(self, app: Flask):
        self.app = app

    def register(self):

        @self.app.get("/")
        def index():
            return render_template("index.html")

        @self.app.get("/admin")
        def admin():
            return render_template("admin.html")

        @self.app.get("/admin/members")
        def admin_members():
            return render_template("admin-members.html")

        @self.app.get("/admin/nodes")
        def admin_nodes():
            return render_template("admin-nodes.html")

        @self.app.get("/admin/nodes/<identifier>")
        def admin_node(identifier: str):
            return render_template("admin-node.html", identifier=identifier)

        @self.app.get("/logout")
        def logout():
            return render_template("logout.html")

        @self.app.get("/admin/account")
        def admin_account():
            return render_template("admin-account.html")

        @self.app.get("/about")
        def about():
            return render_template("about.html")

        @self.app.get("/join")
        def join():
            return render_template("join.html")

        @self.app.get("/login")
        def login():
            return render_template("login.html")

        @self.app.get("/home")
        def home():
            return render_template("home.html")

        @self.app.get("/actor/account")
        def actor_account():
            return render_template("actor-account.html")

        @self.app.get("/messaging/inboxes")
        def inboxes():
            return render_template("inboxes.html")

        @self.app.get("/messaging/outboxes")
        def outboxes():
            return render_template("outboxes.html")
