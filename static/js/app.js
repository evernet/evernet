function displayError(parent, message) {
    parent.html(`<div class="alert alert-danger" role="alert">${message}</div>`);
}

function displaySuccess(parent, message) {
    parent.html(`<div class="alert alert-success" role="alert">${message}</div>`);
}

function getAdminHeaders() {
    return {
        "Authorization": "Bearer " + localStorage.getItem("adminToken"),
    }
}

function getActorHeaders() {
    return {
        "Authorization": "Bearer " + localStorage.getItem("actorToken"),
    }
}

function getNodeIdentifier() {
    return localStorage.getItem("nodeIdentifier");
}

function callIsInit(success, error) {
    $.ajax({
        type: "GET",
        url: "/api/v1/admins/init",
        success: success,
        error: error,
    })
}

function callAdminInit(identifier, password, success, error) {
    $.ajax({
        type: "POST",
        url: "/api/v1/admins/init",
        data: JSON.stringify({identifier: identifier, password: password}),
        success: success,
        error: error,
    })
}

function callAdminLogin(identifier, password, success, error) {
    $.ajax({
        type: "POST",
        url: "/api/v1/admins/token",
        data: JSON.stringify({identifier: identifier, password: password}),
        success: success,
        error: error,
    })
}

function callCreateNode(identifier, displayName, description, success, error) {
    $.ajax({
        type: "POST",
        url: "/api/v1/nodes",
        data: JSON.stringify({identifier: identifier, display_name: displayName, description: description}),
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callFetchNodes(page, size, success, error) {
    $.ajax({
        type: "GET",
        url: "/api/v1/nodes",
        data: {
            page: page,
            size: size,
        },
        success: success,
        error: error,
    })
}

function callGetNode(identifier, success, error) {
    $.ajax({
        type: "GET",
        url: `/api/v1/nodes/${identifier}`,
        success: success,
        error: error,
    })
}

function callUpdateNode(identifier, displayName, description, success, error) {
    $.ajax({
        type: "PUT",
        url: `/api/v1/nodes/${identifier}`,
        data: JSON.stringify({display_name: displayName, description: description}),
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callDeleteNode(identifier, success, error) {
    $.ajax({
        type: "DELETE",
        url: `/api/v1/nodes/${identifier}`,
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callCreateAdmin(identifier, success, error) {
    $.ajax({
        type: "POST",
        url: "/api/v1/admins",
        data: JSON.stringify({identifier: identifier}),
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callFetchAdmins(page, size, success, error) {
    $.ajax({
        type: "GET",
        url: "/api/v1/admins",
        data: {
            page: page,
            size: size,
        },
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callResetAdminPassword(identifier, success, error) {
    $.ajax({
        type: "PUT",
        url: `/api/v1/admins/${identifier}/password`,
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callDeleteAdmin(identifier, success, error) {
    $.ajax({
        type: "DELETE",
        url: `/api/v1/admins/${identifier}`,
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callGetCurrentAdmin(success, error) {
    $.ajax({
        type: "GET",
        url: "/api/v1/admins/current",
        success: success,
        error: error,
        headers: getAdminHeaders(),
    })
}

function callChangeAdminPassword(password, success, error) {
    $.ajax({
        type: "PUT",
        url: `/api/v1/admins/current/password`,
        data: JSON.stringify({password: password}),
        headers: getAdminHeaders(),
        success: success,
        error: error,
    })
}

function callActorSignup(nodeIdentifier, identifier, password, type, displayName, description, success, error) {
    $.ajax({
        type: "POST",
        url: `/api/v1/nodes/${nodeIdentifier}/actors/signup`,
        data: JSON.stringify({identifier: identifier, password: password, type: type, display_name: displayName, description: description}),
        success: success,
        error: error,
    })
}

function callActorToken(nodeIdentifier, identifier, password, success, error) {
    $.ajax({
        type: "POST",
        url: `/api/v1/nodes/${nodeIdentifier}/actors/token`,
        data: JSON.stringify({identifier: identifier, password: password}),
        success: success,
        error: error,
    })
}

function callGetCurrentActor(nodeIdentifier, success, error) {
    $.ajax({
        type: "GET",
        url: `/api/v1/nodes/${nodeIdentifier}/actors/current`,
        headers: getActorHeaders(),
        success: success,
        error: error,
    })
}

function callUpdateCurrentActor(nodeIdentifier, displayName, description, type, success, error) {
    $.ajax({
        type: "PUT",
        url: `/api/v1/nodes/${nodeIdentifier}/actors/current`,
        data: JSON.stringify({type: type, display_name: displayName, description: description}),
        headers: getActorHeaders(),
        success: success,
        error: error,
    })
}

function callChangeCurrentActorPassword(nodeIdentifier, password, success, error) {
    $.ajax({
        type: "PUT",
        url: `/api/v1/nodes/${nodeIdentifier}/actors/current/password`,
        data: JSON.stringify({password: password}),
        headers: getActorHeaders(),
        success: success,
        error: error,
    })
}

function callCreateOutbox(nodeIdentifier, identifier, displayName, description, success, error) {
    $.ajax({
        type: "POST",
        url: `/api/v1/nodes/${nodeIdentifier}/messaging/outboxes`,
        data: JSON.stringify({identifier: identifier, display_name: displayName, description: description}),
        headers: getActorHeaders(),
        success: success,
        error: error,
    })
}

function callFetchOutboxes(nodeIdentifier, page, size, success, error) {
    $.ajax({
        type: "GET",
        url: `/api/v1/nodes/${nodeIdentifier}/messaging/outboxes`,
        data: {
            page: page,
            size: size,
        },
        headers: getActorHeaders(),
        success: success,
        error: error,
    })
}

function callCreateInbox(nodeIdentifier, identifier, displayName, description, success, error) {
    $.ajax({
        type: "POST",
        url: `/api/v1/nodes/${nodeIdentifier}/messaging/inboxes`,
        data: JSON.stringify({identifier: identifier, display_name: displayName, description: description}),
        headers: getActorHeaders(),
        success: success,
        error: error,
    })
}

function callFetchInboxes(nodeIdentifier, page, size, success, error) {
    $.ajax({
        type: "GET",
        url: `/api/v1/nodes/${nodeIdentifier}/messaging/inboxes`,
        data: {
            page: page,
            size: size,
        },
        headers: getActorHeaders(),
        success: success,
        error: error,
    })
}
