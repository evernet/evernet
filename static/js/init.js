$.ajaxSetup({
    complete: function(xhr) {
        if (xhr.status === 401) {
            window.location.href = "/logout.html";
        }
    }
});