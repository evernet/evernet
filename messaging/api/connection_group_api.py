from flask import Flask

from lib.api import authenticate_actor, required_param, optional_param, page, size
from messaging.service import ConnectionGroupService


class ConnectionGroupApi:
    def __init__(self, app: Flask, connection_group_service: ConnectionGroupService):
        self.app = app
        self.connection_group_service = connection_group_service

    def register(self):

        @self.app.post("/api/v1/nodes/<node_identifier>/messaging/connection-groups")
        @authenticate_actor()
        def create_connection_group(actor, node_identifier):
            return self.connection_group_service.create(
                required_param("identifier"),
                required_param("display_name"),
                optional_param("description"),
                node_identifier,
                actor["address"]
            )

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/connection-groups")
        @authenticate_actor()
        def fetch_connection_groups(actor, node_identifier):
            return self.connection_group_service.fetch(
                node_identifier,
                actor["address"],
                page(),
                size()
            )

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/connection-groups/<connection_group_identifier>")
        @authenticate_actor()
        def get_connection_group(actor, node_identifier, connection_group_identifier):
            return self.connection_group_service.get(connection_group_identifier, node_identifier, actor["address"])

        @self.app.put("/api/v1/nodes/<node_identifier>/messaging/connection-groups/<connection_group_identifier>")
        @authenticate_actor()
        def update_connection_group(actor, node_identifier, connection_group_identifier):
            return self.connection_group_service.update(
                connection_group_identifier,
                optional_param("display_name"),
                optional_param("description"),
                node_identifier,
                actor["address"]
            )

        @self.app.delete("/api/v1/nodes/<node_identifier>/messaging/connection-groups/<connection_group_identifier>")
        @authenticate_actor()
        def delete_connection_group(actor, node_identifier, connection_group_identifier):
            return self.connection_group_service.delete(connection_group_identifier, node_identifier, actor["address"])
