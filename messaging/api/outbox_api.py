from flask import Flask

from lib.api import authenticate_actor, required_param, optional_param, size, page
from messaging.service import OutboxService


class OutboxApi:
    def __init__(self, app: Flask, outbox_service: OutboxService):
        self.app = app
        self.outbox_service = outbox_service

    def register(self):

        @self.app.post("/api/v1/nodes/<node_identifier>/messaging/outboxes")
        @authenticate_actor()
        def create_outbox(actor, node_identifier):
            return self.outbox_service.create(
                required_param("identifier"),
                required_param("display_name"),
                optional_param("description"),
                node_identifier,
                actor["address"]
            )

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/outboxes")
        @authenticate_actor()
        def fetch_outboxes(actor, node_identifier):
            return self.outbox_service.fetch(node_identifier, actor["address"], page(), size())

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/outboxes/<outbox_identifier>")
        @authenticate_actor()
        def get_outbox(actor, node_identifier, outbox_identifier):
            return self.outbox_service.get(outbox_identifier, node_identifier, actor["address"])

        @self.app.put("/api/v1/nodes/<node_identifier>/messaging/outboxes/<outbox_identifier>")
        @authenticate_actor()
        def update_outbox(actor, node_identifier, outbox_identifier):
            return self.outbox_service.update(
                outbox_identifier,
                optional_param("display_name"),
                optional_param("description"),
                node_identifier,
                actor["address"]
            )

        @self.app.delete("/api/v1/nodes/<node_identifier>/messaging/outboxes/<outbox_identifier>")
        @authenticate_actor()
        def delete_outbox(actor, node_identifier, outbox_identifier):
            return self.outbox_service.delete(outbox_identifier, node_identifier, actor["address"])
