from flask import Flask

from lib.api import authenticate_node, required_param
from messaging.service import ReceiverService


class ReceiverApi:
    def __init__(self, app: Flask, receiver_service: ReceiverService):
        self.app = app
        self.receiver_service = receiver_service

    def register(self):

        @self.app.post("/api/v1/nodes/<node_identifier>/messaging/receiver/receive")
        @authenticate_node
        def receive_receiver_packet(node, node_identifier):
            return self.receiver_service.receive(
                required_param("packet", dict),
                node_identifier,
                node["address"]
            )