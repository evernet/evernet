from flask import Flask

from lib.api import authenticate_actor, required_param, optional_param, size, page
from messaging.service import InboxService


class InboxApi:
    def __init__(self, app: Flask, inbox_service: InboxService):
        self.app = app
        self.inbox_service = inbox_service

    def register(self):

        @self.app.post("/api/v1/nodes/<node_identifier>/messaging/inboxes")
        @authenticate_actor()
        def create_inbox(actor, node_identifier):
            return self.inbox_service.create(
                required_param("identifier"),
                required_param("display_name"),
                optional_param("description"),
                node_identifier,
                actor["address"]
            )

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/inboxes")
        @authenticate_actor()
        def fetch_inboxes(actor, node_identifier):
            return self.inbox_service.fetch(node_identifier, actor["address"], page(), size())

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/inboxes/<inbox_identifier>")
        @authenticate_actor()
        def get_inbox(actor, node_identifier, inbox_identifier):
            return self.inbox_service.get(inbox_identifier, node_identifier, actor["address"])

        @self.app.put("/api/v1/nodes/<node_identifier>/messaging/inboxes/<inbox_identifier>")
        @authenticate_actor()
        def update_inbox(actor, node_identifier, inbox_identifier):
            return self.inbox_service.update(
                inbox_identifier,
                optional_param("display_name"),
                optional_param("description"),
                node_identifier,
                actor["address"]
            )

        @self.app.delete("/api/v1/nodes/<node_identifier>/messaging/inboxes/<inbox_identifier>")
        @authenticate_actor()
        def delete_inbox(actor, node_identifier, inbox_identifier):
            return self.inbox_service.delete(inbox_identifier, node_identifier, actor["address"])
