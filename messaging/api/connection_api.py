from flask import Flask

from messaging.service import ConnectionService


class ConnectionApi:
    def __init__(self, app: Flask, connection_service: ConnectionService):
        self.app = app
        self.connection_service = connection_service

    def register(self):
        pass