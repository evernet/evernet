from flask import Flask, request

from lib.api import authenticate_actor, required_param, optional_param, page, size
from messaging.service import TopicService


class TopicApi:

    def __init__(self, app: Flask, topic_service: TopicService):
        self.app = app
        self.topic_service = topic_service

    def register(self):

        @self.app.post("/api/v1/nodes/<node_identifier>/messaging/topics")
        @authenticate_actor()
        def create_topic(actor, node_identifier):
            return self.topic_service.create(
                required_param('identifier'),
                required_param('display_name'),
                optional_param('description'),
                optional_param('partition_keys', list),
                required_param("data_schemas", list),
                required_param("whitelist", dict),
                required_param("parent_type"),
                required_param("parent_identifier"),
                node_identifier,
                actor["address"]
            )

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/topics")
        @authenticate_actor()
        def fetch_topics(actor, node_identifier):
            return self.topic_service.fetch(
                request.args.get("parent_type"),
                request.args.get("parent_identifier"),
                node_identifier,
                actor["address"],
                page(),
                size()
            )

        @self.app.get("/api/v1/nodes/<node_identifier>/messaging/topics/<topic_identifier>")
        @authenticate_actor()
        def get_topic(actor, node_identifier, topic_identifier):
            return self.topic_service.get(
                topic_identifier,
                request.args.get("parent_type"),
                request.args.get("parent_identifier"),
                node_identifier,
                actor["address"],
            )

        @self.app.put("/api/v1/nodes/<node_identifier>/messaging/topics/<topic_identifier>")
        @authenticate_actor()
        def update_topic(actor, node_identifier, topic_identifier):
            return self.topic_service.update(
                topic_identifier,
                optional_param("display_name"),
                optional_param("description"),
                optional_param("partition_keys", list),
                optional_param("data_schemas", list),
                optional_param("whitelist", dict),
                request.args.get("parent_type"),
                request.args.get("parent_identifier"),
                node_identifier,
                actor["address"],
            )

        @self.app.delete("/api/v1/nodes/<node_identifier>/messaging/topics/<topic_identifier>")
        @authenticate_actor()
        def delete_topic(actor, node_identifier, topic_identifier):
            return self.topic_service.delete(
                topic_identifier,
                request.args.get("parent_type"),
                request.args.get("parent_identifier"),
                node_identifier,
                actor["address"],
            )
