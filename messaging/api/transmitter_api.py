from flask import Flask

from lib.api import authenticate_actor, required_param
from messaging.service import TransmitterService

class TransmitterApi:
    def __init__(self, app: Flask, transmitter: TransmitterService):
        self.app = app
        self.transmitter_service = transmitter

    def register(self):

        @self.app.post("/api/v1/nodes/<node_identifier>/messaging/transmitter/send")
        @authenticate_actor()
        def send_to_transmitter(actor, node_identifier):
           return self.transmitter_service.send(
               required_param("topic_identifier"),
               required_param("inbox_addresses"),
               required_param("message_schema_address"),
               required_param("message_payload", dict),
               node_identifier,
               actor["address"]
           )
