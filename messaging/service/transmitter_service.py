from node.service import RemoteNodeService
from schema.service.schema_validation_service import SchemaValidationService
from .receiver_service import ReceiverService
from .topic_service import TopicService
from lib.address import decode_actor_address, encode_topic_address


class TransmitterService:
    def __init__(self, topic_service: TopicService, receiver_service: ReceiverService, remote_node_service: RemoteNodeService, schema_validation_service: SchemaValidationService, vertex: str):
        self.topic_service = topic_service
        self.receiver_service = receiver_service
        self.vertex = vertex
        self.schema_validation_service = schema_validation_service
        self.remote_node_service = remote_node_service

    def send(
            self, outbox_identifier: str, outbox_topic_identifier: str,
            inbox_addresses: list[str], message_schema_address: str, message_payload: dict,
            sender_node_identifier: str, sender_actor_address: str
    ) -> dict:
        topic = self.topic_service.get(outbox_topic_identifier, sender_node_identifier, sender_actor_address)

        vertex_to_receivers_map = {}

        for receiver_address in inbox_addresses:
            receiver_actor_identifier, receiver_node_identifier, receiver_vertex = decode_actor_address(
                receiver_address)

            if receiver_vertex not in vertex_to_receivers_map:
                vertex_to_receivers_map[receiver_vertex] = []

            vertex_to_receivers_map[receiver_vertex].append({"actor_address": receiver_address})

        sends = []

        valid_schema, errors = self.schema_validation_service.validate(message_schema_address, message_payload)
        if not valid_schema:
            raise Exception(f"Schema validation failed, errors are {errors}")

        for receiver_vertex, receivers in vertex_to_receivers_map.items():
            packet = {
                "receivers": receivers,
                "sender": {
                    "topic_address": encode_topic_address(topic["identifier"], topic["node_identifier"], self.vertex),
                    "actor_address": sender_actor_address
                },
                "message": {
                    "schema": message_schema_address,
                    "payload": message_payload
                }
            }

            if receiver_vertex == self.vertex:
                result = self.local_send(packet, sender_node_identifier)

                if "sends" in result:
                    sends.extend(result["sends"])
            else:
                result = self.remote_send(receiver_vertex, packet, sender_node_identifier)
                if "sends" in result:
                    sends.extend(result["sends"])

        return {
            "sends": sends
        }

    def local_send(self, packet: dict, sender_node_identifier: str) -> dict:
        receiver_node_identifier_to_receivers_map = self.__construct_receiver_node_identifier_to_receivers_map(packet)
        sender_node_address = f"{self.vertex}/{sender_node_identifier}"

        sends = []
        for receiver_node_identifier, receivers in receiver_node_identifier_to_receivers_map.items():
            packet = {
                "receivers": receivers,
                "sender": packet["sender"],
                "message": packet["message"]
            }

            result = self.receiver_service.receive(packet, receiver_node_identifier, sender_node_address)
            sends.extend(result["sends"])

        return {
            "sends": sends
        }

    def remote_send(self, receiver_vertex, packet: dict, sender_node_identifier: str) -> dict:
        receiver_node_identifier_to_receivers_map = self.__construct_receiver_node_identifier_to_receivers_map(packet)

        sends = []
        for receiver_node_identifier, receivers in receiver_node_identifier_to_receivers_map.items():
            packet = {
                "receivers": receivers,
                "sender": packet["sender"],
                "message": packet["message"]
            }

            result = self.remote_node_service.send_packet_to_receiveer(receiver_vertex, receiver_node_identifier, packet, sender_node_identifier)
            sends.extend(result["sends"])

        return {
            "sends": sends
        }

    @staticmethod
    def __construct_receiver_node_identifier_to_receivers_map(packet: dict) -> dict:
        receivers = packet["receivers"]
        receiver_node_identifier_to_receivers_map = {}
        for receiver in receivers:
            receiver_actor_identifier, receiver_node_identifier, receiver_vertex_endpoint = decode_actor_address(
                receiver["actor_address"])
            if receiver_node_identifier not in receiver_node_identifier_to_receivers_map:
                receiver_node_identifier_to_receivers_map[receiver_node_identifier] = []
            receiver_node_identifier_to_receivers_map[receiver_node_identifier].append(receiver)

        return receiver_node_identifier_to_receivers_map
