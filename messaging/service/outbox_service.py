from pymongo.collection import Collection
from datetime import datetime


class OutboxService:

    def __init__(self, mongo: Collection):
        self.mongo = mongo

    def create(self, identifier: str, display_name: str, description: str, node_identifier: str, actor_address: str) -> dict:
        if not identifier.isalnum():
            raise Exception("Outbox identifier must be alphanumeric")
        if len(identifier) < 3 or len(identifier) > 64:
            raise Exception("Outbox identifier must be between 3 and 64 characters")

        if self.identifier_exists(identifier, node_identifier):
            raise Exception(f"Outbox {identifier} already exists on node {node_identifier}")

        outbox_id = self.mongo.insert_one({
            "identifier": identifier,
            "display_name": display_name,
            "description": description,
            "actor_address": actor_address,
            "node_identifier": node_identifier,
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
        }).inserted_id

        return {
            "id": str(outbox_id),
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        }

    def fetch(self, node_identifier: str, actor_address: str, page=0, size=50) -> list[dict]:
        outboxes = self.mongo.find({
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }).skip(page * size).limit(size)

        result = []
        for outbox in outboxes:
            result.append(self.to_dict(outbox))
        return result

    def get(self, identifier: str, node_identifier: str, actor_address: str) -> dict:
        outbox = self.mongo.find_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        })

        if not outbox:
            raise Exception(f"Outbox {identifier} not found on node {node_identifier}")

        return self.to_dict(outbox)

    def update(self, identifier: str, display_name: str, description: str, node_identifier: str, actor_address: str) -> dict:
        fields = {
            "updated_at": datetime.now()
        }

        if display_name:
            fields["display_name"] = display_name

        fields["description"] = description

        result = self.mongo.update_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Outbox {identifier} not found on node {node_identifier}")

        return {
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        }

    def delete(self, identifier: str, node_identifier: str, actor_address: str) -> dict:
        result = self.mongo.delete_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        })

        if result.deleted_count == 0:
            raise Exception(f"Outbox {identifier} not found on node {node_identifier}")

        return {
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }

    def identifier_exists(self, identifier: str, node_identifier: str) -> bool:
        return self.mongo.count_documents({
            "identifier": identifier,
            "node_identifier": node_identifier,
        }) != 0

    def exists(self, identifier: str, node_identifier: str, actor_address: str) -> bool:
        return self.mongo.count_documents({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }) != 0

    @staticmethod
    def to_dict(self):
        return {
            "identifier": self["identifier"],
            "display_name": self["display_name"],
            "description": self["description"],
            "node_identifier": self["node_identifier"],
            "actor_address": self["actor_address"],
            "created_at": self["created_at"],
            "updated_at": self["updated_at"],
        }
