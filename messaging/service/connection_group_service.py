from datetime import datetime

from pymongo.collection import Collection


class ConnectionGroupService:
    def __init__(self, mongo: Collection):
        self.mongo = mongo

    def create(self, identifier: str, display_name: str, description: str, node_identifier: str, actor_address: str) -> dict:
        if not identifier.isalnum():
            raise Exception("Identifier must be alphanumeric")
        if  len(identifier) < 3 or len(identifier) > 64:
            raise Exception("Identifier must be between 3 and 64 characters")

        if self.identifier_exists(identifier, node_identifier, actor_address):
            raise Exception(f"Connection group {identifier} already exists")

        connection_group_id = self.mongo.insert_one({
            "identifier": identifier,
            "display_name": display_name,
            "description": description,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
        }).inserted_id

        return {
            "id": str(connection_group_id),
            "identifier": identifier
        }

    def fetch(self, node_identifier:str, actor_address: str, page=0, size=50) -> list[dict]:
        connection_groups = self.mongo.find({
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }).skip(page * size).limit(size)

        result = []

        for connection_group in connection_groups:
            result.append(self.to_dict(connection_group))

        return result

    def get(self, identifier: str, node_identifier: str, actor_address: str) -> dict:
        connection_group = self.mongo.find_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        })

        if not connection_group:
            raise Exception(f"Connection group {identifier} not found")

        return self.to_dict(connection_group)

    def update(self, identifier: str, display_name: str, description: str, node_identifier: str, actor_address: str) -> dict:
        fields = {
            "description": description,
            "updated_at": datetime.now(),
        }

        if display_name:
            fields["display_name"] = display_name

        result = self.mongo.update_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Connection group {identifier} not found")

        return {
            "identifier": identifier,
        }

    def delete(self, identifier: str, node_identifier: str, actor_address: str) -> dict:
        result = self.mongo.delete_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        })

        if result.deleted_count == 0:
            raise Exception(f"Connection group {identifier} not found")

        return {
            "identifier": identifier
        }

    def identifier_exists(self, identifier: str, node_identifier: str, actor_address: str) -> bool:
        return self.mongo.count_documents({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }) != 0

    def all_exist(self, identifiers: list[str], node_identifier: str, actor_address: str) -> bool:
        return self.mongo.count_documents({
            "identifier": {"$in": identifiers},
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }) == len(identifiers)

    @staticmethod
    def to_dict(self):
        return {
            "identifier": self["identifier"],
            "display_name": self["display_name"],
            "description": self["description"],
            "node_identifier": self["node_identifier"],
            "actor_address": self["actor_address"],
            "created_at": self["created_at"],
            "updated_at": self["updated_at"],
        }
