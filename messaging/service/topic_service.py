from datetime import datetime

from pymongo.collection import Collection

from messaging.service import InboxService, OutboxService
from messaging.service.connection_group_service import ConnectionGroupService
from messaging.service.connection_service import ConnectionService


class TopicService:
    def __init__(self, mongo: Collection, inbox_service: InboxService, outbox_service: OutboxService, connection_service: ConnectionService, connection_group_service: ConnectionGroupService):
        self.mongo = mongo
        self.inbox_service = inbox_service
        self.outbox_service = outbox_service
        self.connection_service = connection_service
        self.connection_group_service = connection_group_service

    def create(
            self, identifier: str, display_name: str, description: str,
            partition_keys: list[str] | None, data_schemas: list[str], whitelist: dict,
            parent_type: str, parent_identifier: str, node_identifier: str, actor_address: str
    ) -> dict:
        if not identifier.isalnum():
            raise Exception("Topic identifier is not alphanumeric")
        if len(identifier) < 3 or len(identifier) > 64:
            raise Exception("Topic identifier must be between 3 and 64 characters")

        if self.identifier_exists(identifier, parent_type, parent_identifier, node_identifier, actor_address):
            raise Exception(f"Topic {identifier} already exists")

        if parent_type == "inbox":
            if not self.inbox_service.exists(parent_identifier, node_identifier, actor_address):
                raise Exception(f"Inbox {parent_identifier} not found")
        elif parent_type == "outbox":
            if not self.outbox_service.exists(parent_identifier, node_identifier, actor_address):
                raise Exception(f"Outbox {parent_identifier} not found")
        else:
            raise Exception(f"Invalid parent type {parent_type}")

        validated_whitelist = self.validate_whitelist(whitelist, node_identifier, actor_address)
        topic_id = self.mongo.insert_one({
            "identifier": identifier,
            "display_name": display_name,
            "description": description,
            "partition_keys": partition_keys,
            "data_schemas": data_schemas,
            "whitelist": validated_whitelist,
            "parent_type": parent_type,
            "parent_identifier": parent_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
        }).inserted_id

        return {
            "id": str(topic_id),
            "identifier": str(topic_id),
        }

    def fetch(self, parent_type: str, parent_identifier: str, node_identifier: str, actor_address: str, page=0, size=50) -> list[dict]:
        topics = self.mongo.find({
            "parent_type": parent_type,
            "parent_identifier": parent_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        }).skip(page * size).limit(size)

        result = []

        for topic in topics:
            result.append(self.to_dict(topic))

        return result

    def get(self, identifier: str, parent_type: str, parent_identifier: str, node_identifier: str, actor_address: str) -> dict:
        topic = self.mongo.find_one({
            "identifier": identifier,
            "parent_type": parent_type,
            "parent_identifier": parent_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        })

        if not topic:
            raise Exception(f"Topic {identifier} not found")

        return self.to_dict(topic)

    def update(
            self, identifier: str,
            display_name: str, description: str, partition_keys: list[str] | None, data_schemas: list[str], whitelist: dict,
            parent_type: str, parent_identifier: str, node_identifier: str, actor_address: str
    ) -> dict:
        fields = {
            "description": description,
            "partition_keys": partition_keys,
            "updated_at": datetime.now(),
        }

        if display_name:
            fields["display_name"] = display_name

        if data_schemas and len(data_schemas) > 0:
            fields["data_schemas"] = data_schemas

        if whitelist:
            validated_whitelist = self.validate_whitelist(whitelist, node_identifier, actor_address)
            fields["whitelist"] = validated_whitelist

        result = self.mongo.update_one({
            "identifier": identifier,
            "parent_type": parent_type,
            "parent_identifier": parent_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Topic {identifier} not found")

        return {
            "identifier": identifier
        }


    def delete(self, identifier: str, parent_type: str, parent_identifier: str, node_identifier: str, actor_address: str) -> dict:
        result = self.mongo.delete_one({
            "identifier": identifier,
            "parent_type": parent_type,
            "parent_identifier": parent_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        })

        if result.deleted_count == 0:
            raise Exception(f"Topic {identifier} not found")

        return {
            "identifier": identifier
        }

    def identifier_exists(self, identifier: str, parent_type: str, parent_identifier: str, node_identifier: str, actor_address: str) -> bool:
        return self.mongo.count_documents({
            "identifier": identifier,
            "parent_type": parent_type,
            "parent_identifier": parent_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }) != 0

    def match(self, schema: str, parent_type: str, parent_identifier: str, node_identifier: str, actor_address: str) -> list[dict]:
        topics = self.mongo.find({
            "data_schemas": schema,
            "parent_type": parent_type,
            "parent_identifier": parent_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        })

        result = []

        for topic in topics:
            result.append(topic)

        return result

    @staticmethod
    def to_dict(self):
        return {
            "identifier": self["identifier"],
            "display_name": self["display_name"],
            "description": self["description"],
            "partition_keys": self["partition_keys"],
            "data_schemas": self["data_schemas"],
            "whitelist": self["whitelist"],
            "parent_type": self["parent_type"],
            "parent_identifier": self["parent_identifier"],
            "node_identifier": self["node_identifier"],
            "actor_address": self["actor_address"],
            "created_at": self["created_at"],
            "updated_at": self["updated_at"],
        }

    def validate_whitelist(self, whitelist, node_identifier: str, actor_address: str) -> dict:
        if "all" not in whitelist:
            raise Exception("Whitelist is missing all flag")
        if not isinstance(whitelist['all'], bool):
            raise Exception("Whitelist contains invalid all flag")

        if "connection_groups" not in whitelist:
            raise Exception("Whitelist is missing connection groups list")
        if not isinstance(whitelist["connection_groups"], list):
            raise Exception("Whitelist contains invalid connection groups list")

        if "connections" not in whitelist:
            raise Exception("Whitelist is missing connections list")
        if not isinstance(whitelist["connections"], list):
            raise Exception("Whitelist contains invalid connections list")

        if len(whitelist["connections"]) > 0:
            if not self.connection_service.all_exist(whitelist["connections"], node_identifier, actor_address):
                raise Exception(f"Connections {whitelist['connections']} not found")

        if len(whitelist["connection_groups"]) > 0:
            if not self.connection_group_service.all_exist(whitelist["connection_groups"], node_identifier, actor_address):
                raise Exception(f"Connection groups {whitelist['connection_groups']} not found")

        return {
            "all": whitelist['all'],
            "connection_groups": whitelist['connections'],
            "connections": whitelist['connections']
        }
