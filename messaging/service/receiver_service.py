from lib.address import decode_topic_address, decode_node_address, decode_actor_address
from .topic_service import TopicService
from .delivery_service import DeliveryService
from schema.service import SchemaValidationService


class ReceiverService:
    def __init__(
            self,
            topic_service: TopicService,
            schema_validation_service: SchemaValidationService,
            delivery_service: DeliveryService,
            vertex: str
    ):
        self.topic_service = topic_service
        self.schema_validation_service = schema_validation_service
        self.delivery_service = delivery_service
        self.vertex = vertex

    def receive(self, packet: dict, receiver_node_identifier: str, sender_node_address: str) -> dict:
        packet = self.validate_packet(packet)

        if not self.validate_sender_topic(packet, sender_node_address):
            raise Exception("Sender topic mismatch detected")

        if not self.validate_receivers(packet, receiver_node_identifier):
            raise Exception("Received addresses mismatch detected")

        message = packet["message"]
        valid_schema, errors = self.schema_validation_service.validate(message["schema"], message["payload"])
        if not valid_schema:
            raise Exception(f"Schema validation failed, errors are {errors}")

        receiver_topics = self.get_receiver_topics(packet, receiver_node_identifier)

        return self.delivery_service.deliver(packet, receiver_topics)

    @staticmethod
    def validate_packet(packet: dict) -> dict:
        validated_packet = {}
        if "receivers" not in packet:
            raise Exception("Receivers not found in packet")
        receivers = packet["receivers"]
        if not isinstance(receivers, list):
            raise Exception("Receivers must be a list")
        validated_receivers = []
        for receiver in receivers:
            if not isinstance(receiver, dict):
                raise Exception("Invalid receiver")
            if "actor_address" not in receiver:
                raise Exception("Receiver does not contain actor address")
            if not isinstance(receiver["actor_address"], str):
                raise Exception("Invalid actor address in receiver")
            validated_receivers.append({
                "actor_address": receiver["actor_address"],
            })
        validated_packet["receivers"] = validated_receivers

        if "sender" not in packet:
            raise Exception("Sender not found in packet")
        sender = packet["sender"]
        if not isinstance(sender, dict):
            raise Exception("Invalid sender")
        if "actor_address" not in sender:
            raise Exception("Actor address not found in sender")
        if not isinstance(sender["actor_address"], str):
            raise Exception("Invalid actor address in sender")
        if "topic_address" not in sender:
            raise Exception("Topic address not found in sender")
        if not isinstance(sender["topic_address"], str):
            raise Exception("Invalid topic address in sender")
        validated_packet["sender"] = {
            "actor_address": sender["actor_address"],
            "topic_address": sender["topic_address"],
        }

        if "message" not in packet:
            raise Exception("Message not found in packet")
        message = packet["message"]
        if not isinstance(message, dict):
            raise Exception("Invalid message")
        if "schema" not in message:
            raise Exception("Schema not found in message")
        if not isinstance(message["schema"], str):
            raise Exception("Invalid schema in message")
        if "payload" not in message:
            raise Exception("Payload not found in message")
        if not isinstance(message["payload"], dict):
            raise Exception("Invalid payload in message")
        validated_packet["message"] = {
            "schema": message["schema"],
            "payload": message["payload"],
        }

        return validated_packet

    @staticmethod
    def validate_sender_topic(packet: dict, sender_node_address: str) -> bool:
        sender = packet["sender"]
        sender_topic_address = sender["topic_address"]
        topic_identifier, topic_node_identifier, topic_vertex_endpoint = decode_topic_address(sender_topic_address)
        sender_node_identifier, sender_vertex_endpoint = decode_node_address(sender_node_address)
        return topic_node_identifier == sender_node_identifier and topic_vertex_endpoint == sender_vertex_endpoint

    def validate_receivers(self, packet: dict, receiver_node_identifier: str) -> bool:
        receivers = packet["receivers"]
        for receiver in receivers:
            actor_address = receiver["actor_address"]
            actor_identifier, node_identifier, vertex = decode_actor_address(actor_address)
            if node_identifier != receiver_node_identifier or vertex != self.vertex:
               return False
        return True

    def get_receiver_topics(self, packet: dict, receiver_node_identifier: str) -> list[dict]:
        receivers = packet["receivers"]
        message = packet["message"]

        receiver_topics = []

        for receiver in receivers:
            actor_address = receiver["actor_address"]
            actor_identifier, node_identifier, vertex = decode_actor_address(actor_address)
            topics = self.topic_service.match(message["schema"], actor_identifier, receiver_node_identifier)
            receiver_topics.extend(topics)

        return receiver_topics
