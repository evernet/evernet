from datetime import datetime

from pymongo.collection import Collection

from .connection_group_service import ConnectionGroupService


class ConnectionService:
    def __init__(self, mongo: Collection, connection_group_service: ConnectionGroupService):
        self.mongo = mongo
        self.connection_group_service = connection_group_service

    def create(self, identifier: str, display_name: str, description: str, connection_type: str, connection_address: str, connection_group_identifier: str, node_identifier: str, actor_address: str) -> dict:
        if connection_type not in ["inbox", "outbox"]:
            raise Exception(f"Invalid connection type {connection_address}")

        if self.identifier_exists(identifier, connection_group_identifier, node_identifier, actor_address):
            raise Exception(f"Connection {identifier} already exists")

        if not self.connection_group_service.identifier_exists(connection_group_identifier, node_identifier, actor_address):
            raise Exception(f"Connection group {connection_group_identifier} not found")

        connection_id = self.mongo.insert_one({
            "identifier": identifier,
            "display_name": display_name,
            "description": description,
            "type": connection_type,
            "address": connection_address,
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
        }).inserted_id

        return {
            "id": str(connection_id),
            "identifier": identifier,
            "node_identifier": node_identifier,
        }

    def fetch(self, connection_group_identifier: str, node_identifier: str, actor_address: str, page=0, size=50) -> list[dict]:
        connections = self.mongo.find({
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }).skip(page * size).limit(size)

        result = []

        for connection in connections:
            result.append(self.to_dict(connection))

        return result

    def get(self, identifier: str, connection_group_identifier: str, node_identifier: str, actor_address: str) -> dict:
        connection = self.mongo.find_one({
            "identifier": identifier,
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        })

        if not connection:
            raise Exception(f"Connection {identifier} not found in {connection_group_identifier}")

        return self.to_dict(connection)

    def update(self, identifier: str, display_name: str, description: str, connection_group_identifier: str, node_identifier: str, actor_address: str):
        fields = {
            "description": description,
            "updated_at": datetime.now(),
        }

        if display_name:
            fields["display_name"] = display_name

        result = self.mongo.update_one({
            "identifier": identifier,
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Connection {identifier} not found in {connection_group_identifier}")

        return {
            "identifier": identifier,
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier,
        }

    def delete(self, identifier: str, connection_group_identifier: str, node_identifier: str, actor_address: str):
        result = self.mongo.delete_one({
            "identifier": identifier,
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        })

        if result.deleted_count == 0:
            raise Exception(f"Connection {identifier} not found in {connection_group_identifier}")

        return {
            "identifier": identifier,
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier
        }

    def identifier_exists(self, identifier: str, connection_group_identifier: str, node_identifier: str, actor_address: str):
        return self.mongo.count_documents({
            "identifier": identifier,
            "connection_group_identifier": connection_group_identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }) != 0

    def all_exist(self, identifiers: list[str], node_identifier: str, actor_address: str) -> bool:
        return self.mongo.count_documents({
            "identifier": {"$in": identifiers},
            "node_identifier": node_identifier,
            "actor_address": actor_address
        }) == len(identifiers)

    @staticmethod
    def to_dict(self):
        return {
            "identifier": self["identifier"],
            "display_name": self["display_name"],
            "description": self["description"],
            "type": self["type"],
            "address": self["address"],
            "connection_group_identifier": self["connection_group_identifier"],
            "node_identifier": self["node_identifier"],
            "actor_address": self["actor_address"],
            "created_at": self["created_at"],
            "updated_at": self["updated_at"]
        }
