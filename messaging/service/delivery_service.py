from uuid import UUID


class DeliveryService:
    def __init__(self):
        pass

    def deliver(self, packet: dict, receiver_topics: list[dict]) -> dict:
        if len(receiver_topics) == 0:
            return {
                "sends": []
            }

        receiver_packets = []
        sends = []

        for receiver_topic in receiver_topics:
            packet_id = str(UUID())
            receiver_packets.append({
                "id": str(packet_id),
                "sender":packet["sender"],
                "message":packet["message"],
                "receiver": {
                    "topic_identifier": receiver_topic["identifier"],
                    "node_identifier": receiver_topic["node_identifier"]
                }
            })
            sends.append({
                "id": packet_id,
            })

        self.deliver_packets(receiver_packets)

        return {
            "sends": sends
        }

    def deliver_packets(self, receiver_packets: list[dict]):
        print(f"Delivering packets {receiver_packets}")
