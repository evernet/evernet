from lib.address import decode_schema_address
from node.service import RemoteNodeService
from schema.service import SchemaRegistryService


class SchemaValidationService:
    def __init__(self, schema_registry_service: SchemaRegistryService, remote_node_service: RemoteNodeService, vertex: str):
        self.schema_registry_service = schema_registry_service
        self.remote_node_service = remote_node_service
        self.vertex = vertex

    def validate(self, schema_address: str, data: object) -> (bool, list[str]):
        schema_definition = self.get_schema_definition(schema_address)
        return self.validate_data("root", data, schema_definition["definition"])

    @staticmethod
    def validate_data(field: str, data: object, schema_definition: dict) -> (bool, list[str]):
        data_type = schema_definition["data_type"]
        required = schema_definition["required"]

        if required and not data:
            return False, [f"Required data is missing for field {field}"]

        if not data:
            return True, []

        if data_type == "string":
            if not isinstance(data, str):
                return False, [f"Field {field} must be a string"]
        elif data_type == "integer":
            if not isinstance(data, int):
                return False, [f"Field {field} must be a integer"]
        elif data_type == "float":
            if not isinstance(data, float):
                return False, [f"Field {field} must be a float"]
        elif data_type == "boolean":
            if not isinstance(data, bool):
                return False, [f"Field {field} must be a boolean"]
        elif data_type == "array":
            if not isinstance(data, list):
                return False, [f"Field {field} must be an array"]
            element_schema = schema_definition["element_definition"]
            errors = []
            success = True
            for el in data:
                el_success, el_errors = SchemaValidationService.validate_data(field, el, element_schema)
                if not el_success:
                    success = False
                    errors.extend(el_errors)
            if not success:
                return success, errors
        elif data_type == "object":
            if not isinstance(data, dict):
                return False, [f"Field {field} must be an object"]
            fields_schema = schema_definition["fields_definition"]
            errors = []
            success = True
            for k, v in data.items():
                field_schema = fields_schema[k]
                field_success, field_errors = SchemaValidationService.validate_data(k, v, field_schema)
                if not field_success:
                    success = False
                    errors.extend(field_errors)
            if not success:
                return success, errors

        return True, []

    def get_schema_definition(self, schema_address: str) -> dict:
        schema_identifier, schema_node_identifier, schema_vertex = decode_schema_address(schema_address)

        if schema_vertex == self.vertex:
            schema_definition = self.schema_registry_service.get(schema_identifier, schema_node_identifier)
        else:
            schema_definition = self.remote_node_service.get_schema_definition(schema_vertex, schema_node_identifier, schema_identifier)

        return schema_definition