from datetime import datetime

from pymongo.collection import Collection


class SchemaRegistryService:
    def __init__(self, mongo: Collection):
        self.mongo = mongo

    def create(self, identifier: str, definition: dict, node_identifier: str, actor_address: str):
        if self.identifier_exists(identifier, node_identifier):
            raise Exception(f"Schema {identifier} already exists on node {node_identifier}")

        schema_registry_id = self.mongo.insert_one({
            "identifier": identifier,
            "definition": self.validate_definition(definition),
            "node_identifier": node_identifier,
            "actor_address": actor_address,
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
        }).inserted_id

        return {
            "id": str(schema_registry_id),
            "identifier": identifier,
            "node_identifier": node_identifier,
        }

    def fetch(self, node_identifier: str, page=0, size=50) -> list[dict]:
        schemas = self.mongo.find({
            "node_identifier": node_identifier,
        }).skip(page * size).limit(size)

        result = []
        for schema in schemas:
            result.append(self.to_dict(schema))
        return result

    def fetch_for_actor(self, actor_address: str, node_identifier: str, page=0, size=50) -> list[dict]:
        schemas = self.mongo.find({
            "actor_address": actor_address,
            "node_identifier": node_identifier,
        }).skip(page * size).limit(size)

        result = []
        for schema in schemas:
            result.append(self.to_dict(schema))
        return result

    def get(self, identifier: str, node_identifier: str) -> dict:
        schema = self.mongo.find_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
        })

        if not schema:
            raise Exception(f"Schema {identifier} not found on node {node_identifier}")

        return self.to_dict(schema)

    def update(self, identifier: str, definition: dict, node_identifier: str, actor_address: str) -> dict:
        fields = {
            "updated_at": datetime.now(),
        }

        if definition:
            fields["definition"] = self.validate_definition(definition)

        result = self.mongo.update_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        }, {
            "$set": fields,
        })

        if result.matched_count == 0:
            raise Exception(f"Schema {identifier} not found on node {node_identifier}")

        return {
            "identifier": identifier,
            "node_identifier": node_identifier,
        }

    def delete(self, identifier: str, node_identifier: str, actor_address: str) -> dict:
        result = self.mongo.delete_one({
            "identifier": identifier,
            "node_identifier": node_identifier,
            "actor_address": actor_address,
        })

        if result.deleted_count == 0:
            raise Exception(f"Schema {identifier} not found on node {node_identifier}")

        return {
            "identifier": identifier,
            "node_identifier": node_identifier,
        }

    def identifier_exists(self, identifier: str, node_identifier: str) -> bool:
        return self.mongo.count_documents({
            "identifier": identifier,
            "node_identifier": node_identifier
        }) != 0

    @staticmethod
    def validate_definition(definition: dict) -> dict:
        return SchemaRegistryService.validate_field_schema("root", definition)

    @staticmethod
    def validate_field_schema(field: str, field_schema: dict) -> dict:
        validated_field_schema = {}
        if "data_type" not in field_schema:
            raise Exception(f"Data type not specified for field {field}")
        data_type = field_schema["data_type"]
        if data_type not in ["string", "integer", "float", "boolean", "object", "array"]:
            raise Exception(f"Invalid data type {data_type} specified for field {field}")
        validated_field_schema["data_type"] = data_type

        required_flag = False
        if "required" in field_schema:
            required_flag = field_schema["required"]
            if not isinstance(required_flag, bool):
                raise Exception(f"Invalid required flag specified for field {field}")
        validated_field_schema["required"]  = required_flag

        description = None
        if "description" in field_schema:
            description = field_schema["description"]
            if isinstance(description, str):
                raise Exception(f"Invalid description specified for field {field}")
        validated_field_schema["description"] = description

        if data_type == "object":
            if "fields_definition" not in field_schema:
                raise Exception(f"Fields definition not specified for object field {field}")
            fields_definition = field_schema["fields_definition"]
            if not isinstance(fields_definition, dict):
                raise Exception(f"Invalid fields definition specified for object field {field}")
            validated_fields_definition = {}
            for field, field_schema in fields_definition:
                validated_fields_definition[field] = SchemaRegistryService.validate_field_schema(field, field_schema)
            validated_field_schema["fields_definition"] = validated_fields_definition

        if data_type == "array":
            if "element_definition" not in field_schema:
                raise Exception(f"Element definition not specified for array field {field}")
            element_schema = field_schema["element_definition"]
            if not isinstance(element_schema, dict):
                raise Exception(f"Invalid element schema specified for array field {field}")
            validated_field_schema["element_definition"] = SchemaRegistryService.validate_field_schema("element_definition", element_schema)

        return validated_field_schema

    @staticmethod
    def to_dict(self):
        return {
            "identifier": self["identifier"],
            "definition": self["definition"],
            "node_identifier": self["node_identifier"],
            "actor_address": self["actor_address"],
            "created_at": self["created_at"],
            "updated_at": self["updated_at"],
        }
