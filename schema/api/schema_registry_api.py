from flask import Flask

from lib.api import authenticate_actor, required_param, size, page
from schema.service import SchemaRegistryService


class SchemaRegistryApi:
    def __init__(self, app: Flask, schema_registry_service: SchemaRegistryService):
        self.app = app
        self.schema_registry_service = schema_registry_service

    def register(self):

        @self.app.post("/api/v1/nodes/<node_identifier>/schemas")
        @authenticate_actor()
        def create_schema(actor, node_identifier):
            return self.schema_registry_service.create(
                required_param("identifier"),
                required_param("definition", dict),
                node_identifier,
                actor["address"]
            )

        @self.app.get("/api/v1/nodes/<node_identifier>/schemas")
        def fetch_schemas(node_identifier):
            return self.schema_registry_service.fetch(node_identifier, page(), size())

        @self.app.get("/api/v1/nodes/<node_identifier>/schemas/registered")
        @authenticate_actor()
        def fetch_actor_schemas(actor, node_identifier):
            return self.schema_registry_service.fetch_for_actor(actor["address"], node_identifier, page(), size())

        @self.app.get("/api/v1/nodes/<node_identifier>/schemas/<schema_identifier>")
        def get_schema(node_identifier, schema_identifier):
            return self.schema_registry_service.get(schema_identifier, node_identifier)

        @self.app.put("/api/v1/nodes/<node_identifier>/schemas/<schema_identifier>")
        @authenticate_actor()
        def update_schema(actor, node_identifier, schema_identifier):
            return self.schema_registry_service.update(
                schema_identifier,
                required_param("definition", dict),
                node_identifier,
                actor["address"]
            )

        @self.app.delete("/api/v1/nodes/<node_identifier>/schemas/<schema_identifier>")
        @authenticate_actor()
        def delete_schema(actor, node_identifier, schema_identifier):
            return self.schema_registry_service.delete(schema_identifier, node_identifier, actor["address"])
