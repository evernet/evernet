import requests

from node.service import NodeService


class RemoteNodeService:
    def __init__(self, federation_protocol: str, node_service: NodeService):
        self.federation_protocol = federation_protocol
        self.node_service = node_service

    def get(self, vertex_endpoint: str, identifier: str) -> dict:
        response = requests.get(f"{self.federation_protocol}://{vertex_endpoint}/api/v1/nodes/{identifier}")

        if not response.ok:
            raise Exception(f"Failed to fetch node details for {identifier} from {vertex_endpoint}, error is {response.text}")

        response = response.json()

        return {
            "identifier": response["identifier"],
            "display_name": response["display_name"],
            "signing_public_key": response["signing_public_key"],
            "description": response["description"],
            "creator": response["creator"],
            "created_on": response["created_on"],
            "modified_on": response["modified_on"]
        }

    def send_packet_to_receiveer(self, vertex_endpoint: str, node_identifier: str, packet: dict, sender_node_identifier:  str) -> dict:
        response = requests.post(f"{self.federation_protocol}://{vertex_endpoint}/api/v1/nodes/{node_identifier}/messaging/receiver/receive", json={
            "packet": packet,
        }, headers={
            "Authorization": f"Bearer {self.node_service.get_federation_token(sender_node_identifier, f"{vertex_endpoint}/{node_identifier}")}"
        })

        if not response.ok:
            raise Exception(f"Failed to send packet to receiver for {node_identifier} at {vertex_endpoint}, error is {response.text}")

        response = response.json()

        return {
            "sends": response["sends"],
        }

    def get_schema_definition(self, vertex_endpoint: str, node_identifier: str, schema_identifier: str) -> dict:
        response = requests.get(f"{self.federation_protocol}://{vertex_endpoint}/api/v1/nodes/{node_identifier}/schemas/{schema_identifier}")

        if not response.ok:
            raise Exception(f"Failed to fetch schema definition for {schema_identifier} from {vertex_endpoint}, error is {response.text}")

        response = response.json()

        return {
            "identifier": response["identifier"],
            "definition": response["definition"],
            "node_identifier": response["node_identifier"],
            "actor_address": response["actor_address"],
            "created_at": response["created_at"],
            "updated_at": response["updated_at"],
        }