import jwt
from cryptography.hazmat.primitives.asymmetric import ed25519
from pymongo.collection import Collection
from datetime import datetime
import time

from lib.address import encode_node_address
from lib.ed25519 import generate_ed25519_keys, private_key_to_string, public_key_to_string, string_to_private_key


class NodeService:
    def __init__(self, mongo: Collection, vertex: str):
        self.mongo = mongo
        self.vertex = vertex

    def create(self, identifier: str, display_name: str, description: str, creator: str) -> dict:
        if not identifier.isalnum():
            raise Exception("Identifier must be alphanumeric")
        if len(identifier) < 3 or len(identifier) > 64:
            raise Exception("Identifier must be between 3 and 64 characters")

        if self.mongo.count_documents({
            "identifier": identifier,
        }) != 0:
            raise Exception(f"Node {identifier} already exists")

        signing_private_key, signing_public_key = generate_ed25519_keys()

        self.mongo.insert_one({
            "identifier": identifier,
            "display_name": display_name,
            "description": description,
            "signing_private_key": private_key_to_string(signing_private_key),
            "signing_public_key": public_key_to_string(signing_public_key),
            "creator": creator,
            "created_at": datetime.now(),
            "updated_at": datetime.now(),
        })

        return {
            "identifier": identifier,
        }

    def fetch(self, page=0, size=50) -> list[dict]:
        nodes = self.mongo.find({}).skip(page * size).limit(size)
        result = []
        for node in nodes:
            result.append(self.to_dict(node))
        return result

    def get(self, identifier: str) -> dict:
        node = self.mongo.find_one({"identifier": identifier})
        if not node:
            raise Exception(f"Node {identifier} not found")
        return self.to_dict(node)

    def update(self, identifier: str, display_name: str, description: str) -> dict:
        fields = {
            "description": description,
            "updated_at": datetime.now(),
        }

        if display_name:
            fields["display_name"] = display_name

        result = self.mongo.update_one({
            "identifier": identifier,
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Node {identifier} not found")

        return {
            "identifier": identifier,
        }

    def delete(self, identifier: str) -> dict:
        result = self.mongo.delete_one({"identifier": identifier})
        if result.deleted_count == 0:
            raise Exception(f"Node {identifier} not found")
        return {
            "identifier": identifier,
        }

    def reset_signing_key(self, identifier: str) -> dict:
        signing_private_key, signing_public_key = generate_ed25519_keys()
        fields = {
            "signing_private_key": private_key_to_string(signing_private_key),
            "signing_public_key": public_key_to_string(signing_public_key),
            "updated_at": datetime.now(),
        }

        result = self.mongo.update_one({
            "identifier": identifier,
        }, {
            "$set": fields
        })

        if result.matched_count == 0:
            raise Exception(f"Node {identifier} not found")

        return {
            "identifier": identifier,
        }

    def get_signing_private_key(self, identifier: str) -> ed25519.Ed25519PrivateKey:
        node = self.mongo.find_one({
            "identifier": identifier
        })

        if not node:
            raise Exception(f"Node {identifier} not found")

        return string_to_private_key(node["signing_private_key"])

    def get_federation_token(self, identifier: str, target_node_address: str) -> dict:
        node = self.get(identifier)

        signing_private_key = string_to_private_key(node["signing_private_key"])

        federation_token = jwt.encode({
            "sub": node["identifier"],
            "type": "node",
            "iss": encode_node_address(node["identifier"], self.vertex),
            "aud": target_node_address,
            "iat": int(time.time()),
        }, headers={
            "kid": encode_node_address(node["identifier"], self.vertex),
        }, algorithm="EdDSA", key=signing_private_key)

        return {
            "token": federation_token
        }

    def exists(self, identifier: str) -> bool:
        return self.mongo.count_documents({
            "identifier": identifier
        }) != 0

    @staticmethod
    def to_dict(self):
        return {
            "identifier": self["identifier"],
            "display_name": self["display_name"],
            "description": self["description"],
            "signing_public_key": self["signing_public_key"],
            "creator": self["creator"],
            "created_at": self["created_at"],
            "updated_at": self["updated_at"],
        }
