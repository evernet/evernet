import os
import pymongo
from dotenv import *
from flask import Flask

from actor.api import ActorApi
from actor.service import ActorService
from messaging.api import TopicApi, TransmitterApi, InboxApi, OutboxApi
from messaging.api.receiver_api import ReceiverApi
from messaging.service import TopicService, TransmitterService, ReceiverService, DeliveryService, InboxService, \
    OutboxService, ConnectionGroupService, ConnectionService
from node.api import HealthApi, AdminApi, NodeApi
from lib.api import register_api_handlers
from node.service import AdminService, NodeService
from node.service.node_key_service import NodeKeyService
from node.service.remote_node_service import RemoteNodeService
from schema.api import SchemaRegistryApi
from schema.service import SchemaRegistryService, SchemaValidationService
from web import WebRoutes

app = Flask(__name__)
load_dotenv()

jwt_signing_key = os.getenv("JWT_SIGNING_KEY")
vertex = os.getenv("VERTEX_ENDPOINT")
federation_protocol = os.getenv("FEDERATION_PROTOCOL")

db = pymongo.MongoClient(os.getenv("DB_HOST"), int(os.getenv("DB_PORT"))).evernet

admin_service = AdminService(db.admins, jwt_signing_key, vertex)
node_service = NodeService(db.nodes, vertex)
remote_node_service = RemoteNodeService(federation_protocol, node_service)
node_key_service = NodeKeyService(node_service, remote_node_service, vertex)
actor_service = ActorService(db.actors, node_service, vertex)
schema_registry_service = SchemaRegistryService(db.schema_registries)
schema_validation_service = SchemaValidationService(schema_registry_service, remote_node_service, vertex)
inbox_service = InboxService(db.inboxes)
outbox_service = OutboxService(db.outboxes)
connection_group_service = ConnectionGroupService(db.connection_groups)
connection_service = ConnectionService(db.connections, connection_group_service)
topic_service = TopicService(db.topics, inbox_service, outbox_service, connection_service, connection_group_service)
delivery_service = DeliveryService()
receiver_service = ReceiverService(topic_service, schema_validation_service, delivery_service, vertex)
transmitter_service = TransmitterService(topic_service, receiver_service, remote_node_service, schema_validation_service, vertex)

HealthApi(app, vertex).register()
AdminApi(app, admin_service).register()
NodeApi(app, node_service).register()
ActorApi(app, actor_service).register()
SchemaRegistryApi(app, schema_registry_service).register()
InboxApi(app, inbox_service).register()
OutboxApi(app, outbox_service).register()
TopicApi(app, topic_service).register()
TransmitterApi(app, transmitter_service).register()
ReceiverApi(app, receiver_service).register()

WebRoutes(app).register()

register_api_handlers(app, node_service, node_key_service, jwt_signing_key, vertex)

if __name__ == '__main__':
    app.run(
        host=os.getenv("HOST"),
        port=int(os.getenv("PORT")),
        debug=os.getenv("ENV") != "PROD"
    )
